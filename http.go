package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"fcms-alerts/alerts"

	"github.com/gorilla/mux"
)

func newHTTPHandler(service alerts.Service) http.Handler {
	h := &httpService{service}
	r := mux.NewRouter()
	r.HandleFunc("/alerts", h.CreateAlert).Methods("POST")
	r.HandleFunc("/alerts/{user_id}", h.ListUserAlerts).Methods("GET")
	return withLog(r)
}

func withLog(h http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Print(r.Method, " ", r.URL.Path)
		h.ServeHTTP(w, r)
	}
}

type httpService struct {
	service alerts.Service
}

func (h *httpService) CreateAlert(w http.ResponseWriter, r *http.Request) {
	var req alerts.CreateAlertRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, err)
		return
	}
	result, err := h.service.CreateAlert(r.Context(), &req)
	if err != nil {
		handleError(w, err)
		return
	}
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(result)
}

func (h *httpService) ListUserAlerts(w http.ResponseWriter, r *http.Request) {
	userID := mux.Vars(r)["user_id"]
	result, err := h.service.ListUserAlerts(r.Context(), userID)
	if err != nil {
		handleError(w, err)
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(result)
}

func handleError(w http.ResponseWriter, err error) {
	var verr alerts.ValidationError
	switch {
	case errors.Is(err, alerts.ErrNotFound):
		w.WriteHeader(http.StatusNotFound)
	case errors.As(err, &verr):
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(verr)
	default:
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintln(w, err)
	}
}
