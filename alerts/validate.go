package alerts

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/google/uuid"
)

// ValidationError allows to gather errors on multiple fields and present them
// as a JSON object.
type ValidationError map[string]string

func (v ValidationError) Unwrap() error {
	return ErrInvalidInput
}

func (v ValidationError) Error() string {
	data, _ := v.MarshalJSON()
	return fmt.Sprintf("invalid input: %s", data)
}

func (v ValidationError) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]string(v))
}

func (v ValidationError) Add(field string, err error) {
	v[field] = err.Error()
}

var (
	errMissingMandatoryField = errors.New("missing mandatory field")
	errInvalidUUID           = errors.New("invalid UUID")
	errInconstentRange       = errors.New("inconsistent range (max < min)")
)

func (c *CreateAlertRequest) Validate() (*Alert, error) {
	verr := make(ValidationError)

	var err error
	var alert Alert

	if alert.UserID, err = validateUUID(c.UserID); err != nil {
		verr.Add("user_id", err)
	}
	if alert.Name, err = validateString(c.Name); err != nil {
		verr.Add("name", err)
	}
	if alert.City, err = validateString(c.City); err != nil {
		verr.Add("city", err)
	}
	if c.PriceMin != nil && c.PriceMax != nil {
		if *c.PriceMax < *c.PriceMin {
			verr.Add("price_max", errInconstentRange)
		}
	}
	alert.PriceMin = c.PriceMin
	alert.PriceMax = c.PriceMax

	if len(verr) != 0 {
		return nil, verr
	}
	return &alert, nil
}

func ValidateUserID(input string) (uuid.UUID, error) {
	userID, err := validateUUID(input)
	if err != nil {
		verr := make(ValidationError)
		verr.Add("user_id", err)
		return userID, verr
	}
	return userID, nil
}

func validateUUID(input string) (uuid.UUID, error) {
	if input == "" {
		return uuid.Nil, errMissingMandatoryField
	}
	id, err := uuid.Parse(input)
	if err != nil {
		return uuid.Nil, errInvalidUUID
	}
	return id, nil

}

func validateString(input string) (string, error) {
	if input == "" {
		return "", errMissingMandatoryField
	}
	return input, nil
}
