package alerts

import (
	"context"
	"sync"

	"github.com/google/uuid"
)

func NewFakeStore() Store {
	return &fakeStore{
		byUserID: make(map[uuid.UUID][]Alert),
	}
}

type fakeStore struct {
	byUserID map[uuid.UUID][]Alert
	mtx      sync.Mutex
}

func (f *fakeStore) Insert(_ context.Context, alert *Alert) error {
	f.mtx.Lock()
	defer f.mtx.Unlock()

	userAlerts := f.byUserID[alert.UserID]
	userAlerts = append(userAlerts, *alert)
	f.byUserID[alert.UserID] = userAlerts
	return nil
}

func (f *fakeStore) ListUserAlerts(_ context.Context, userID uuid.UUID) ([]Alert, error) {
	f.mtx.Lock()
	defer f.mtx.Unlock()

	alerts, ok := f.byUserID[userID]
	if !ok {
		return nil, ErrNotFound
	}
	result := make([]Alert, len(alerts))
	copy(result, alerts)
	return result, nil
}
