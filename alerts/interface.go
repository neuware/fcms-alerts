package alerts

import (
	"context"
	"errors"
	"time"

	"github.com/google/uuid"
)

var (
	// ErrNotFound is returned when no alerts are registered for a user.
	ErrNotFound = errors.New("not found")

	// ErrInvalidInput is returned upon input validation errors.
	ErrInvalidInput = errors.New("invalid input")
)

// Service is the core alerts service implementation.
type Service interface {
	CreateAlert(ctx context.Context, in *CreateAlertRequest) (*Alert, error)
	ListUserAlerts(ctx context.Context, userID string) ([]Alert, error)
}

// Store is a database-agnostic store for user alerts.
type Store interface {
	Insert(context.Context, *Alert) error
	ListUserAlerts(context.Context, uuid.UUID) ([]Alert, error)
}

// Alert models an user alert on newly published real estate offers.
type Alert struct {
	ID        uuid.UUID `json:"id"`
	UserID    uuid.UUID `json:"user_id"`
	CreatedAt time.Time `json:"created_at"`
	Name      string    `json:"name"`
	City      string    `json:"city"`
	PriceMin  *uint     `json:"price_min,omitempty"`
	PriceMax  *uint     `json:"price_max,omitempty"`
}

// CreateAlertRequest is the data required to create a new Alert.
type CreateAlertRequest struct {
	UserID   string `json:"user_id"`
	Name     string `json:"name"`
	City     string `json:"city"`
	PriceMin *uint  `json:"price_min,omitempty"`
	PriceMax *uint  `json:"price_max,omitempty"`
}
