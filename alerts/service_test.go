package alerts

import (
	"testing"

	"github.com/google/uuid"
)

func TestCreateAlert(t *testing.T) {
	type checkFunc func(*Alert, error) error

	noError := func(_ *Alert, err error) error { return noError(err) }
	isError := func(want error) checkFunc {
		return func(_ *Alert, err error) error { return isError(want, err) }
	}
	hasInvalidFields := func(fields ...string) checkFunc {
		return func(_ *Alert, err error) error { return hasInvalidFields(err, fields...) }
	}
	isValidAlert := func(alert *Alert, _ error) error { return isValidAlert(alert) }
	all := func(checks ...checkFunc) checkFunc {
		return func(got *Alert, err error) error {
			for _, check := range checks {
				if err := check(got, err); err != nil {
					return err
				}
			}
			return nil
		}
	}

	price := func(value uint) *uint { return &value }

	testCases := []struct {
		Desc  string
		Input CreateAlertRequest
		Check checkFunc
	}{
		{
			Desc: "empty input",
			Check: all(
				isError(ErrInvalidInput),
				hasInvalidFields("user_id", "name", "city"),
			),
		},
		{
			Desc: "invalid user id",
			Input: CreateAlertRequest{
				UserID: "not a uuid",
				Name:   "anything in Paris",
				City:   "Paris",
			},
			Check: all(
				isError(ErrInvalidInput),
				hasInvalidFields("user_id"),
			),
		},
		{
			Desc: "inconsistent price range",
			Input: CreateAlertRequest{
				UserID:   uuid.NewString(),
				Name:     "something good in Paris",
				City:     "Paris",
				PriceMin: price(1337),
				PriceMax: price(42),
			},
			Check: all(
				isError(ErrInvalidInput),
				hasInvalidFields("price_max"),
			),
		},
		{
			Desc: "nominal",
			Input: CreateAlertRequest{
				UserID:   uuid.NewString(),
				Name:     "cheap studio in Lyon",
				City:     "Lyon",
				PriceMin: price(400),
				PriceMax: price(700),
			},
			Check: all(
				noError,
				isValidAlert,
			),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Desc, func(t *testing.T) {
			service := NewService(NewFakeStore())
			ctx, cancel := newTestContext()
			defer cancel()

			result, err := service.CreateAlert(ctx, &tc.Input)
			if err := tc.Check(result, err); err != nil {
				t.Errorf("case %q: %s", tc.Desc, err)
			}
		})
	}
}

func TestListUserAlerts(t *testing.T) {
	service := NewService(NewFakeStore())

	ctx, cancel := newTestContext()
	defer cancel()

	t.Run("invalid userID", func(t *testing.T) {
		_, err := service.ListUserAlerts(ctx, "not a uuid")
		expect(t,
			isError(ErrInvalidInput, err),
			hasInvalidFields(err, "user_id"),
		)
	})

	t.Run("unknown userID", func(t *testing.T) {
		_, err := service.ListUserAlerts(ctx, uuid.NewString())
		expect(t,
			isError(ErrNotFound, err),
		)
	})

	t.Run("nominal", func(t *testing.T) {
		userID := uuid.New()
		price := func(value uint) *uint { return &value }

		alerts := make([]Alert, 0, 2)

		alert, err := service.CreateAlert(ctx, &CreateAlertRequest{
			UserID: userID.String(),
			Name:   "anything in Tombuctu",
			City:   "Tombuctu",
		})
		expect(t,
			noError(err),
		)
		alerts = append(alerts, *alert)

		alert, err = service.CreateAlert(ctx, &CreateAlertRequest{
			UserID:   userID.String(),
			Name:     "cheap studio in Pétaouchnok",
			City:     "Pétaouchnok",
			PriceMax: price(500),
		})
		expect(t,
			noError(err),
		)
		alerts = append(alerts, *alert)

		result, err := service.ListUserAlerts(ctx, userID.String())
		expect(t,
			noError(err),
			equal(alerts, result, "alert list"),
		)
	})
}
