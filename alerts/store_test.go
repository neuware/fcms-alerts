package alerts

import (
	"testing"
	"time"

	"github.com/google/uuid"
)

func TestStore(t *testing.T) {
	t.Run("fakeStore", func(t *testing.T) {
		genericTestStore(t, NewFakeStore)
	})
}

func genericTestStore(t *testing.T, newStore func() Store) {
	store := newStore()
	userID := uuid.New()

	var priceMin uint = 42
	var priceMax uint = 1337

	alerts := []Alert{
		{
			ID:        uuid.New(),
			UserID:    userID,
			CreatedAt: time.Now(),
			Name:      "alert 1",
			City:      "Paris",
			PriceMin:  &priceMin,
			PriceMax:  &priceMax,
		},
		{
			ID:        uuid.New(),
			UserID:    userID,
			CreatedAt: time.Now(),
			Name:      "alert 2",
			City:      "Lyon",
		},
	}

	t.Run("insert alerts", func(t *testing.T) {
		for _, alert := range alerts {
			t.Run(alert.Name, func(t *testing.T) {
				ctx, cancel := newTestContext()
				defer cancel()
				err := store.Insert(ctx, &alert)
				expect(t,
					noError(err),
				)
			})
		}
	})

	t.Run("list alerts nominal", func(t *testing.T) {
		ctx, cancel := newTestContext()
		defer cancel()

		result, err := store.ListUserAlerts(ctx, userID)
		expect(t,
			noError(err),
			equal(alerts, result, "user alerts"),
		)
	})

	t.Run("list alerts unknown user", func(t *testing.T) {
		ctx, cancel := newTestContext()
		defer cancel()
		_, err := store.ListUserAlerts(ctx, uuid.New())
		expect(t,
			isError(ErrNotFound, err),
		)
	})
}
