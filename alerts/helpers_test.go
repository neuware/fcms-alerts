package alerts

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/google/uuid"
)

func newTestContext() (context.Context, func()) {
	return context.WithTimeout(context.Background(), 10*time.Second)
}

func expect(t *testing.T, errs ...error) {
	t.Helper()
	for _, err := range errs {
		if err != nil {
			t.Fatal(err)
		}
	}
}

func noError(err error) error {
	if err != nil {
		return fmt.Errorf("unexpected error: %s", err)
	}
	return nil
}

func isError(want, err error) error {
	if err == nil {
		return fmt.Errorf("expected error %q, got nil", want)
	}
	if !errors.Is(err, want) {
		return fmt.Errorf("expected error %q, got %q", want, err)
	}
	return nil
}

func equal(want, got interface{}, what string) error {
	if !reflect.DeepEqual(want, got) {
		return fmt.Errorf("%s: expected %#v, got %#v", what, want, got)
	}
	return nil
}

func isValidAlert(alert *Alert) error {
	if alert.ID == uuid.Nil {
		return fmt.Errorf("alert ID wasn't set")
	}
	if alert.CreatedAt.IsZero() {
		return fmt.Errorf("creation date wasn't set")
	}
	if alert.UserID == uuid.Nil {
		return fmt.Errorf("user ID wasn't set")
	}
	if alert.City == "" {
		return fmt.Errorf("city wasn't set")
	}
	if alert.Name == "" {
		return fmt.Errorf("name wasn't set")
	}
	return nil
}

func hasInvalidFields(err error, fields ...string) error {
	var verr ValidationError
	if errors.As(err, &verr) {
		for _, field := range fields {
			if _, ok := verr[field]; !ok {
				return fmt.Errorf("missing expected error on field %q", field)
			}
		}
		if len(verr) != len(fields) {
			return fmt.Errorf("expected invalid fields %q, got more: %v", fields, verr)
		}
	} else {
		return fmt.Errorf("error doesn't cast to ValidationError: %s", err)
	}
	return nil
}
