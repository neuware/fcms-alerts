package alerts

import (
	"context"
	"time"

	"github.com/google/uuid"
)

func NewService(store Store) Service {
	return &service{store}
}

type service struct {
	store Store
}

func (s *service) CreateAlert(ctx context.Context, in *CreateAlertRequest) (*Alert, error) {
	alert, err := in.Validate()
	if err != nil {
		return nil, err
	}
	alert.ID = uuid.New()
	alert.CreatedAt = time.Now()
	return alert, s.store.Insert(ctx, alert)
}

func (s *service) ListUserAlerts(ctx context.Context, userID string) ([]Alert, error) {
	id, err := ValidateUserID(userID)
	if err != nil {
		return nil, err
	}
	return s.store.ListUserAlerts(ctx, id)
}
