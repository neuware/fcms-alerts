package main

import (
	"fcms-alerts/alerts"
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"
)

var port int

func init() {
	flag.IntVar(&port, "p", 80, "Port to serve to")
	flag.Parse()
}

func main() {
	store := alerts.NewFakeStore()
	service := alerts.NewService(store)
	log.Println("serving on port", port)
	srv := http.Server{
		Handler:      newHTTPHandler(service),
		Addr:         fmt.Sprintf(":%d", port),
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
